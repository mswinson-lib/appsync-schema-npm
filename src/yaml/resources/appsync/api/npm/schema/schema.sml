type Api {
  repository(url: String): Repository
}

type SourceRepository {
  type: String
  url: String
 }

type Person {
  name: String
  email: String
}

type Dependency {
  name: String
  version: String
} 

type Version {
  version: String
  name: String
  description: String
  url: String
  keywords: [String]
  author: Person
  contributors: [Person]
  maintainers: [Person]
  dependencies: [Dependency]
  devDependencies: [Dependency]
  repository: SourceRepository
}

type Bug {
  url: String
}

type Package {
  name: String
  description: String
  readme: String
  homepage: String
  keywords: [String]
  bugs: Bug
  license: String
  versions: [Version]
}

type Repository {
  url: String
  package(name: String) : Package
}

type Query {
  npm: Api 
}

schema {
  query: Query
}

